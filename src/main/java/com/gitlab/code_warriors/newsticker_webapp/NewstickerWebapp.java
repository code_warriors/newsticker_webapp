package com.gitlab.code_warriors.newsticker_webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class NewstickerWebapp {

    public static void main(String[] args) {
        SpringApplication.run(NewstickerWebapp.class, args);
    }

}
