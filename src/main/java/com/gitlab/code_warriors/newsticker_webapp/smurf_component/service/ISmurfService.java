package com.gitlab.code_warriors.newsticker_webapp.smurf_component.service;

import com.gitlab.code_warriors.newsticker_webapp.smurf_component.entity.SmurfEntity;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public interface ISmurfService {

    public Iterable<SmurfEntity> getAllSmurfs();

    public Optional<SmurfEntity> getSmurfBySmurfId(Long smurfId);

}

