
package com.gitlab.code_warriors.newsticker_webapp.smurf_component.repository;

import com.gitlab.code_warriors.newsticker_webapp.smurf_component.entity.SmurfEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISmurfRepository extends CrudRepository<SmurfEntity, Long> {

}
