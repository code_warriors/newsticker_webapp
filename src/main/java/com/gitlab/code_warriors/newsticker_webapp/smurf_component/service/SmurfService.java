package com.gitlab.code_warriors.newsticker_webapp.smurf_component.service;

import com.gitlab.code_warriors.newsticker_webapp.smurf_component.entity.SmurfEntity;
import com.gitlab.code_warriors.newsticker_webapp.smurf_component.repository.ISmurfRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class SmurfService implements ISmurfService {
	
    @Autowired
    private ISmurfRepository smurfRepository;		   

    @Override
    public Iterable<SmurfEntity> getAllSmurfs() {
        Iterable<SmurfEntity> list = smurfRepository.findAll();
        return list;
    }

    @Override
    public Optional<SmurfEntity> getSmurfBySmurfId(Long smurfId) {
        Optional<SmurfEntity> smurf = smurfRepository.findById(smurfId);
        return smurf;
    }
      
}
