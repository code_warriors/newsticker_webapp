package com.gitlab.code_warriors.newsticker_webapp.smurf_component.controller;

import com.gitlab.code_warriors.newsticker_webapp.smurf_component.entity.SmurfEntity;
import com.gitlab.code_warriors.newsticker_webapp.smurf_component.service.ISmurfService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/restapi")
public class SmurfController {

    @Autowired
    private ISmurfService smurfService;

    @GetMapping(path = "/smurf/id/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<SmurfEntity>> index(@PathVariable String id) {

        Optional<SmurfEntity> smurf = smurfService.getSmurfBySmurfId(Long.parseLong(id));

        return new ResponseEntity<Optional<SmurfEntity>>(smurf, HttpStatus.OK);
    }

}
